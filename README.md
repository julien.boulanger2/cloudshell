# CloudShell Tutorial

This tutorial will guide you through the process of setting up your environment for working with a Kubernetes cluster on Google Cloud Platform using CloudShell.

## Prerequisites

Before starting this tutorial, you should have the following:

- A Google Cloud Platform account
- A Kubernetes cluster set up on Google Cloud Platform
- Access to CloudShell

## Steps

1. Open CloudShell.

```sh
export ENV=prod
export KUBECONFIG=~/.kube/gke-lesechos-${ENV}
gcloud config set project oxalide-lesechos-${ENV}
gcloud container clusters get-credentials --zone=europe-west1-b lesechos-${ENV}-europe-west1-b-master
```
